/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;
import Controller.ModelView;
import Utilitaires.*;
import java.util.HashMap;
/**
 *
 * @author iants
 */
public class Dept {
    private int id;
    private String nom;

     @MyUrl(url = "liste_dept.do")
    public ModelView get_dept(){
        System.out.println("Here!!");
        HashMap<String, Object> Hval=new HashMap<>();
        Dept[] all=Dept.get_all();
        ModelView md=new ModelView();
        Hval.put("liste_Dept", all);
        md.setDonnees(Hval);
        md.setPage("result.jsp");
        return md;
    }
    
      @MyUrl(url = "insertDept.do")
    public ModelView insertDept()
    {
        ModelView mv = new ModelView();
        mv.setPage("result.jsp");
        HashMap<String,Object> map = new HashMap<>();
        map.put("Dept", this);
        mv.setDonnees(map);
        return mv;
    }
    
   
    public static Dept[]  get_all(){
        Dept[] val=new Dept[3];
        val[0]=new Dept(1,"Maintenance");
        val[1]=new Dept(2, "Informatique");
        val[2]=new Dept(3,"Developpement");
        return val;
    }
     public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }
    
    @MyUrl(url = "zaaaaajlb.do")
    public void setId(int id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }


    public Dept(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }
    
    
    
}
